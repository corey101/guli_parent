package com.atguigu.msmservice.utils;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class CustomUtil {

    /**
     * @param phone  手机号码
     * @param param  验证码
     * @param minute 有效时间（分钟）
     * @return
     */
    public static boolean sendShortMessage(String phone, Map<String, String> param, int minute) {
        String code = param.get("code");

        ResourceBundle bundle = ResourceBundle.getBundle("properties/shortMessageApi");

        String host = bundle.getString("host");
        String path = bundle.getString("path");
        String method = bundle.getString("method");
        String appcode = bundle.getString("appcode");
        String smsSignId = bundle.getString("smsSignId");
        String templateId = bundle.getString("templateId");
        Map<String, String> headers = new HashMap<>();
        // 最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> queries = new HashMap<>();
        queries.put("mobile", phone);
        queries.put("param", "**code**:" + code + ",**minute**:" + minute);
        queries.put("smsSignId", smsSignId);
        queries.put("templateId", templateId);
        Map<String, String> body = new HashMap<>();

        try {
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, queries, body);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                return true;
            } else {
                String reasonPhrase = statusLine.getReasonPhrase();
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

}
