package com.atguigu.eduservice.entity.vo;

import lombok.Data;

/**
 * Created by corey on 2021/7/23
 **/
@Data
public class SubjectVo {

    private String id;
    private String title;
}
